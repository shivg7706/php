<html>
<style>
input[type=text], select {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
}

input[type=submit] {
  float: left;
  background-color: #4CAF50;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  border-radius: 4px;
  cursor: pointer;
}

input[type=submit]:hover {
  background-color: #45a049;
}

div {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 20px;
}

</style>

<head>
    <meta charset="UTF-8" />
    <title>Search</title>
</head>

<body>
    <?php
    include 'index.php';
    ?>

    <h1>Attendace</h1>
    <form action="results.php" method="post">
        <label>Enrollment No.
            <input type="text" name="en_no" />
        </label>
        <label>Faculty No.
            <input type="text" name="fac_no" />
        </label>
        <input type="submit" value="Submit" /> 
    </form>
</body>

</html>