<?php
include 'db.php';

$EN_NO = $_POST['EN_NO'];
$FAC_NO = $_POST['FAC_NO'];
$NAME = $_POST['NAME'];
$ADDRESS = $_POST['ADDRESS'];
$MOB_NO = $_POST['MOB_NO'];

$stmt = $conn->prepare("INSERT INTO `STUDENT` VALUES (?, ?, ?, ?, ?)");
$stmt->bind_param('sssss', $EN_NO, $FAC_NO, $NAME, $ADDRESS, $MOB_NO);
$stmt->execute();
$stmt->close();

?>