<html>
<style>
input, select {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
}

input[type=submit] {
    float: ;
    align-content: center;
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    border-radius: 4px;
    cursor: pointer;
}

input[type=submit]:hover {
  background-color: #45a049;
}

div {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 20px;
}
</style>
<head>
    <title>Student Data Entries</title>
</head>
<body>
    <?php
    include 'index.php';
    ?>
    <fieldset style="width:70%" align="center">
        <legend>Student</legend>
        <form action="insert.php" method="post">
            <div>
                <label for="display-name">Enrollment no.</label><br>
                <input  type="text"
                        name="EN_NO"
                        placeholder="GJ6423" 
                        maxlength="15" 
                        required />
            </div>
            <div>
                <label for="profession">Faculty No.</label><br>
                <input  type="text"
                        name="FAC_NO"
                        placeholder="16PEB026"  
                        required />
            </div>
            <div>
                <label  for="phone">Name</label><br>
                <input  type="text"
                        name="NAME"
                        placeholder="Shivam Gupta"  
                        required />
            </div>  
            <div>
                <label  for="phone">Address</label><br>
                <input  type="tel"
                        name="ADDRESS"
                        placeholder="ALIGARH"
                        required/>
            </div>  
            <div>
                <label  for="email">Mobile No.</label><br>
                <input  type="tel"
                        placeholder="+91904578181" 
                        name="MOB_NO" />
            </div>           
            <div>
                <input type="submit" id="button" value="SUBMIT">
                <!-- <button type="submit">Submit</button> -->
            </div>
        </form>
    </fieldset>
</body>
</html>