<html>
<head>
<title>Search Results</title>
<style type="text/css">
table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
    background-color: #90ee90;
}
</style>
</head>
<body>

<?php
include("db.php");
include("index.php");

$en_no = $_POST['en_no'];
$fac_no = $_POST['fac_no'];

$sqlStr = "SELECT * FROM `STUDENT` WHERE `EN_NO`='$en_no'";
$sqlRes = " SELECT * FROM `AWARD` WHERE `EN_NO`='$en_no' AND `FAC_NO`='$fac_no'";

$details = mysqli_query($conn, $sqlStr);


echo '<table border="4"  cellspacing="0">
    <tr>
        <th>Enrollment No.</th>
        <th>Faculty No.</th>
        <th>Name</th>
    </tr>';

while ($row = mysqli_fetch_array($details)) {
    echo '
        <tr>
            <td>'.$row['EN_NO'].'</td>
            <td>'.$row['FAC_NO'].'</td>
            <td>'.$row['NAME'].'</td>
        </tr>';

}
echo '
</table>';

echo "<br><br>";

$result = mysqli_query($conn, $sqlRes);

echo '<table border="4"  cellspacing="0">
    <tr>
        <th>Course No.</th>
        <th>Mid Semester Marks</th>
        <th>Total Semester Marks</th>
        <th>Grade</th>
    </tr>';

while ($row = mysqli_fetch_array($result)) {
    echo '
        <tr>
            <td>'.$row['COURSE_ID'].'</td>
            <td>'.$row['MIDS_MARKS'].'</td>
            <td>'.$row['TOTAL_MARKS'].'</td>
            <td>'.$row['GRADE'].'</td>
        </tr>';

}
echo '
</table>';

?> 

</body>
</html>